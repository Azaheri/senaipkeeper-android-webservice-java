package com.android.info.senai.senaipkeeper;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.info.senai.senaipkeeper.configs.RetrofitConfigUsuario;
import com.android.info.senai.senaipkeeper.models.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText editEmail;
    private EditText editSenha;
    private Button btnLogar;
    private Usuario usuario = new Usuario();

    private static Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        MainActivity.activity = this;

        editEmail = findViewById(R.id.etEmail);
        editSenha = findViewById(R.id.etSenha);
        btnLogar = findViewById(R.id.btnLogin);

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usuario.setEmail(editEmail.getText().toString());
                usuario.setSenha(editSenha.getText().toString());
                Call<ResponseBody> call = new RetrofitConfigUsuario().getUsuarioTokenInterface().gerarToken(usuario);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject token = new JSONObject(response.body().string());

                                SharedPreferences sharedPreferences = MainActivity.activity.getSharedPreferences("token", MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("tokenUsuario", "Bearer " + token.get("token").toString());
                                editor.apply();
                                Intent intent = new Intent(MainActivity.this, AmbienteDrawerActivity.class);
                                startActivity(intent);
                                finish();

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (response.errorBody() != null) {
                            Toast.makeText(activity, "Seu e-mail e/ou senha inválidos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("Erro?", t.getMessage());
                        Toast.makeText(MainActivity.this, "Não foi possível fazer login", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }
}
