package com.android.info.senai.senaipkeeper.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.info.senai.senaipkeeper.R;
import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterMovimentacao;
import com.android.info.senai.senaipkeeper.models.Movimentacao;

public class MovimentacaoViewHolder extends RecyclerView.ViewHolder {

    private final RecyclerAdapterMovimentacao adapter;
    private Long id;
    private final TextView idTv;
    private final TextView nome;
    private final TextView data;
    private final TextView usuario;

    public MovimentacaoViewHolder(View itemView, RecyclerAdapterMovimentacao adapter) {
        super(itemView);
        this.adapter = adapter;
        this.nome = itemView.findViewById(R.id.itemNome);
        this.idTv = itemView.findViewById(R.id.itemId);
        this.data = itemView.findViewById(R.id.itemData);
        this.usuario = itemView.findViewById(R.id.itemUsuario);
    }

    public void preencher(Movimentacao movimentacao) {
        id = movimentacao.getId();
        nome.setText(movimentacao.getItemPatrimonio().getPatrimonio().getNome());
        idTv.setText(String.valueOf(id));
        data.setText(movimentacao.getDataMovimentacao());
        usuario.setText(movimentacao.getUsuario().getNome() + " " + movimentacao.getUsuario().getSobrenome());
    }
}
