package com.android.info.senai.senaipkeeper.services;

import com.android.info.senai.senaipkeeper.models.Usuario;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UsuarioService {
    @POST("rest/auth/jwt")
    Call<ResponseBody> gerarToken(@Body Usuario usuario);
}