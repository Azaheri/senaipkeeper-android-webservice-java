package com.android.info.senai.senaipkeeper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterMovimentacao;
import com.android.info.senai.senaipkeeper.configs.RetrofitConfigMovimentacao;
import com.android.info.senai.senaipkeeper.models.Ambiente;
import com.android.info.senai.senaipkeeper.models.ItemPatrimonio;
import com.android.info.senai.senaipkeeper.models.Movimentacao;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovimentacaoDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerLista;
    private RecyclerAdapterMovimentacao recyclerAdapter;
    private List<Movimentacao> movimentacoes = new ArrayList<>();
    private Movimentacao movimentacaoAmbiente = new Movimentacao();
    private String tokenUser;

    private Button btnMovimentar;
    private EditText etItemM;
    private EditText etAmbienteD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimentacao_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final SharedPreferences tokenSp = this.getSharedPreferences("token", MODE_PRIVATE);
        tokenUser = tokenSp.getString("tokenUsuario", null);

        recyclerLista = findViewById(R.id.rvMovimentacao);
        btnMovimentar = findViewById(R.id.btnMovimentar);
        etItemM = findViewById(R.id.etItemM);
        etAmbienteD = findViewById(R.id.etAmbienteD);

        btnMovimentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ambiente ambiente = new Ambiente();
                ItemPatrimonio itemPatrimonio = new ItemPatrimonio();
                String idItemString = etItemM.getText().toString();
                String idAmbienteString = etAmbienteD.getText().toString();
                if (etItemM.getText().toString().isEmpty() || etAmbienteD.getText().toString().isEmpty()) {
                    Toast.makeText(MovimentacaoDrawerActivity.this, "Preencha os campos corretamente", Toast.LENGTH_SHORT).show();
                } else {
                    ambiente.setId(Long.parseLong(idAmbienteString));
                    itemPatrimonio.setId(Long.parseLong(idItemString));
                    movimentacaoAmbiente.setAmbiente_destino(ambiente);
                    movimentacaoAmbiente.setItemPatrimonio(itemPatrimonio);
                    Log.e("Ambiente +Movimentação ", ambiente.toString() + movimentacaoAmbiente.toString());
                    final Call<Movimentacao> listCall = new RetrofitConfigMovimentacao(tokenUser).getMovimentacaoInterface().realizarMovimentacoes(Long.valueOf(idItemString), movimentacaoAmbiente);
                    listCall.enqueue(new Callback<Movimentacao>() {
                        @Override
                        public void onResponse(Call<Movimentacao> call, Response<Movimentacao> response) {
                            if (response.code() == 404) {
                                Toast.makeText(MovimentacaoDrawerActivity.this, "Erro: Reveja os IDs do ambiente e do item desejados", Toast.LENGTH_LONG).show();
                            } else if (response.code() == 422) {
                                Toast.makeText(MovimentacaoDrawerActivity.this, "Erro: O item já está no local desejado, escolha outro", Toast.LENGTH_LONG).show();
                            } else {
                                if (response.isSuccessful()) {
//                                    Log.e("Sucesso ", String.valueOf(response.body()));
                                    Movimentacao movimentacao = response.body();
//                                    Log.e("MovimentacaoMov", String.valueOf(movimentacao));
                                    Toast.makeText(MovimentacaoDrawerActivity.this, "O item " + movimentacao.getItemPatrimonio().getPatrimonio().getNome() + " de ID " +
                                            movimentacao.getItemPatrimonio().getId() + " foi movimentado com sucesso", Toast.LENGTH_LONG).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Movimentacao> call, Throwable t) {
                            Log.e("Falhou", t.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movimentacao_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem movimentacao) {
        int id = movimentacao.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(movimentacao);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem movimentacao) {
        int id = movimentacao.getItemId();

        if (id == R.id.nav_ambiente) {
            Intent intent = new Intent(this, AmbienteDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_item) {
            Intent intent = new Intent(this, ItemDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_movimentacao) {
            Intent intent = new Intent(this, ListaMovimentacaoDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_logout) {
            tokenUser = "";
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
