package com.android.info.senai.senaipkeeper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.info.senai.senaipkeeper.R;
import com.android.info.senai.senaipkeeper.holder.AmbienteViewHolder;
import com.android.info.senai.senaipkeeper.models.Ambiente;

import java.util.List;

public class RecyclerAdapterAmbiente extends RecyclerView.Adapter {
    private List<Ambiente> ambientes;
    private Context context;

    public RecyclerAdapterAmbiente(Context context, List<Ambiente> ambientes) {
        this.ambientes = ambientes;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.lista_de_ambientes, parent, false);
        AmbienteViewHolder holder = new AmbienteViewHolder(view, this);
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AmbienteViewHolder holder2 = (AmbienteViewHolder) holder;
        Ambiente ambiente = ambientes.get(position);
        holder2.preencher(ambiente);
    }

    @Override
    public int getItemCount() {
        if (ambientes == null) {
            return 0;
        }
        return ambientes.size();
    }

    public void setAmbientes(List<Ambiente> ambientes) {
        this.ambientes = ambientes;
    }
}
