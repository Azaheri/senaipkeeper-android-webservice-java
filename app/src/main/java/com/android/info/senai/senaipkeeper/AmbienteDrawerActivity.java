package com.android.info.senai.senaipkeeper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterAmbiente;
import com.android.info.senai.senaipkeeper.configs.RetrofitConfigAmbiente;
import com.android.info.senai.senaipkeeper.models.Ambiente;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AmbienteDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerLista;
    private RecyclerAdapterAmbiente recyclerAdapter;
    private List<Ambiente> ambientes = new ArrayList<>();

    private String tokenUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambiente_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final SharedPreferences tokenSp = this.getSharedPreferences("token", MODE_PRIVATE);
        tokenUser = tokenSp.getString("tokenUsuario", null);
        Log.e("tokenusuario", tokenUser);

        recyclerLista = findViewById(R.id.rvAmbientes);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerLista.setLayoutManager(linearLayoutManager);
        recyclerAdapter = new RecyclerAdapterAmbiente(getApplicationContext(), ambientes);
        recyclerLista.setAdapter(recyclerAdapter);


        final Call<List<Ambiente>> listCall = new RetrofitConfigAmbiente(tokenUser).getAmbienteInterface().listarAmbientes();
        listCall.enqueue(new Callback<List<Ambiente>>() {
            @Override
            public void onResponse(Call<List<Ambiente>> call, Response<List<Ambiente>> response) {
                if (response.isSuccessful()) {
                    ambientes = response.body();
                    if (ambientes != null) {
                        List<Ambiente> aaaaa = response.body();
                        Log.e("sadsadsadsadsadsad", aaaaa.toString());
                        recyclerAdapter.setAmbientes(ambientes);
                        recyclerAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Ambiente>> call, Throwable t) {
                Toast.makeText(AmbienteDrawerActivity.this, "Erro, os ambientes não puderam ser carregados.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ambiente_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_item) {
            Intent intent = new Intent(this, ItemDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_movimentacao) {
            Intent intent = new Intent(this, ListaMovimentacaoDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_rMovimentacao) {
            Intent intent = new Intent(this, MovimentacaoDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_logout) {
            tokenUser = "";
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
