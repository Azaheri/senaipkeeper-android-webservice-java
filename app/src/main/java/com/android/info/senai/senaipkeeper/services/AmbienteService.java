package com.android.info.senai.senaipkeeper.services;

import com.android.info.senai.senaipkeeper.models.Ambiente;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AmbienteService {
    @GET("rest/ambientes")
    Call<List<Ambiente>> listarAmbientes();
}