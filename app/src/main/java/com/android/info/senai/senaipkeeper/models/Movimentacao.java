package com.android.info.senai.senaipkeeper.models;

import java.io.Serializable;

public class Movimentacao implements Serializable {
    private Long id;

    private Ambiente ambiente_origem;

    private Ambiente ambiente_destino;

    private Usuario usuario;

    private ItemPatrimonio itemPatrimonio;

    private String dataMovimentacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ambiente getAmbiente_origem() {
        return ambiente_origem;
    }

    public void setAmbiente_origem(Ambiente ambiente_origem) {
        this.ambiente_origem = ambiente_origem;
    }

    public Ambiente getAmbiente_destino() {
        return ambiente_destino;
    }

    public void setAmbiente_destino(Ambiente ambiente_destino) {
        this.ambiente_destino = ambiente_destino;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ItemPatrimonio getItemPatrimonio() {
        return itemPatrimonio;
    }

    public void setItemPatrimonio(ItemPatrimonio itemPatrimonio) {
        this.itemPatrimonio = itemPatrimonio;
    }

    public String getDataMovimentacao() {
        return dataMovimentacao;
    }

    public void setDataMovimentacao(String dataMovimentacao) {
        this.dataMovimentacao = dataMovimentacao;
    }

    @Override
    public String toString() {
        return "Movimentacao{" +
                "id=" + id +
                ", ambiente_origem=" + ambiente_origem +
                ", ambiente_destino=" + ambiente_destino +
                ", usuario=" + usuario +
                ", itemPatrimonio=" + itemPatrimonio +
                ", dataMovimentacao=" + dataMovimentacao +
                '}';
    }
}
