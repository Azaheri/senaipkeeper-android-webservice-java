package com.android.info.senai.senaipkeeper.configs;

import com.android.info.senai.senaipkeeper.services.UsuarioService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.android.info.senai.senaipkeeper.utils.AppUtils.SPK_BASE_URL;

public class RetrofitConfigUsuario {
    private final Retrofit retrofit;

    public RetrofitConfigUsuario() {
        retrofit = new Retrofit.Builder().baseUrl(SPK_BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public UsuarioService getUsuarioTokenInterface() {
        return retrofit.create(UsuarioService.class);
    }

}