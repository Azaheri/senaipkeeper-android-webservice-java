package com.android.info.senai.senaipkeeper.configs;

import com.android.info.senai.senaipkeeper.services.MovimentacaoService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.android.info.senai.senaipkeeper.utils.AppUtils.SPK_BASE_URL;

public class RetrofitConfigMovimentacao {
    private final Retrofit retrofit;

    public RetrofitConfigMovimentacao(final String token) {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder b = chain.request().newBuilder();
                b.addHeader("Accept", "application/json");
                b.addHeader("Authorization", token);
                return chain.proceed(b.build());
            }
        }).build();

        retrofit = new Retrofit.Builder().baseUrl(SPK_BASE_URL).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public MovimentacaoService getMovimentacaoInterface() {
        return retrofit.create(MovimentacaoService.class);
    }
}
