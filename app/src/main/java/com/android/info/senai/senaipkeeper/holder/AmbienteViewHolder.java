package com.android.info.senai.senaipkeeper.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.info.senai.senaipkeeper.R;
import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterAmbiente;
import com.android.info.senai.senaipkeeper.models.Ambiente;

public class AmbienteViewHolder extends RecyclerView.ViewHolder {
    private final RecyclerAdapterAmbiente adapter;
    private Long id;
    private final TextView nome;
    private final TextView idTv;

    public AmbienteViewHolder(View itemView, RecyclerAdapterAmbiente adapter) {
        super(itemView);
        this.adapter = adapter;
        this.nome = itemView.findViewById(R.id.ambienteNome);
        this.idTv = itemView.findViewById(R.id.ambienteId);
    }

    public void preencher(Ambiente ambiente) {
        id = ambiente.getId();
        nome.setText(ambiente.getNome());
        idTv.setText(String.valueOf(id));
    }
}
