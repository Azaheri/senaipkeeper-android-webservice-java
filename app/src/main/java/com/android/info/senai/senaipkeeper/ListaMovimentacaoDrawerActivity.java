package com.android.info.senai.senaipkeeper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterMovimentacao;
import com.android.info.senai.senaipkeeper.configs.RetrofitConfigMovimentacao;
import com.android.info.senai.senaipkeeper.models.ItemPatrimonio;
import com.android.info.senai.senaipkeeper.models.Movimentacao;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaMovimentacaoDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerLista;
    private RecyclerAdapterMovimentacao recyclerAdapter;
    private List<Movimentacao> movimentacoes = new ArrayList<>();

    private String tokenUser;

    private Button btnBuscarMovimentacao;
    private EditText etItemM;

    private Long id, idMov;
    private TextView idTv;
    private TextView nome;
    private TextView data;
    private TextView usuario;
    private TextView ambiente;
    private TextView movId;

    private ItemPatrimonio itemPatrimonio = new ItemPatrimonio();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_movimentacao_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final SharedPreferences tokenSp = this.getSharedPreferences("token", MODE_PRIVATE);
        tokenUser = tokenSp.getString("tokenUsuario", null);
        Log.e("tokenusuario", tokenUser);

        recyclerLista = findViewById(R.id.rvMovimentacao);
        btnBuscarMovimentacao = findViewById(R.id.btnBuscarMovimentacaoL);
        etItemM = findViewById(R.id.etIdItem);


        nome = findViewById(R.id.itemNome);
        idTv = findViewById(R.id.itemId);
        data = findViewById(R.id.itemData);
        usuario = findViewById(R.id.itemUsuario);
        ambiente = findViewById(R.id.ambientePatrimonio);
        movId = findViewById(R.id.movId);

        btnBuscarMovimentacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etItemM.getText().toString().isEmpty()) {
                    Toast.makeText(ListaMovimentacaoDrawerActivity.this, "Insira o ID do item antes de buscar", Toast.LENGTH_SHORT).show();
                } else {
                    String idItemString = etItemM.getText().toString();
                    Call<Movimentacao> listCall = new RetrofitConfigMovimentacao(tokenUser).getMovimentacaoInterface().listarMovimentacoes(Long.valueOf(idItemString));
                    listCall.enqueue(new Callback<Movimentacao>() {
                        @Override
                        public void onResponse(Call<Movimentacao> call, Response<Movimentacao> response) {
                            if (response.code() == 404) {
                                Toast.makeText(ListaMovimentacaoDrawerActivity.this, "O Item buscado não existe", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e("Código listar mov", String.valueOf(response.code()));
                                if (response.isSuccessful()) {
                                    Movimentacao movimentacao = response.body();
                                    if (movimentacao != null) {
                                        id = movimentacao.getItemPatrimonio().getId();
                                        idMov = movimentacao.getId();
                                        nome.setText(movimentacao.getItemPatrimonio().getPatrimonio().getNome());
                                        idTv.setText(String.valueOf(id));
                                        movId.setText(String.valueOf(idMov));
                                        Log.e("aa", "Bom dia: " + movimentacao.getDataMovimentacao());
                                        data.setText(movimentacao.getDataMovimentacao());
                                        usuario.setText(movimentacao.getUsuario().getNome() + " " + movimentacao.getUsuario().getSobrenome());
                                        ambiente.setText(movimentacao.getItemPatrimonio().getAmbiente().getNome());
                                        Log.e("asdadsdsafio9", movimentacao.toString());
                                    } else {
                                        Log.e("asdadsdsafio9", movimentacao.toString());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Movimentacao> call, Throwable t) {
                            Toast.makeText(ListaMovimentacaoDrawerActivity.this, "Não há movimentação para este item", Toast.LENGTH_LONG).show();
                            Log.e("ERRO DA LISTA MOV", t.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.lista_movimentacao_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_ambiente) {
            Intent intent = new Intent(this, AmbienteDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_item) {
            Intent intent = new Intent(this, ItemDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_rMovimentacao) {
            Intent intent = new Intent(this, MovimentacaoDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_logout) {
            tokenUser = "";
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
