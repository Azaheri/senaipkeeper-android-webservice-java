package com.android.info.senai.senaipkeeper.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.info.senai.senaipkeeper.R;
import com.android.info.senai.senaipkeeper.holder.MovimentacaoViewHolder;
import com.android.info.senai.senaipkeeper.models.Movimentacao;

import java.util.List;

public class RecyclerAdapterMovimentacao extends RecyclerView.Adapter {

    private List<Movimentacao> movimentacoes;
    private Context context;

    public RecyclerAdapterMovimentacao(Context applicationContext, List<Movimentacao> movimentacoes) {
        this.movimentacoes = movimentacoes;
        this.context = applicationContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.lista_de_movimentacoes, parent, false);
        MovimentacaoViewHolder holder = new MovimentacaoViewHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MovimentacaoViewHolder holder2 = (MovimentacaoViewHolder) holder;
        Movimentacao movimentacao = movimentacoes.get(position);
        holder2.preencher(movimentacao);
    }

    @Override
    public int getItemCount() {
        if (movimentacoes == null) {
            return 0;
        }
        return movimentacoes.size();
    }

    public void setMovimentacoes(List<Movimentacao> movimentacoes) {
        this.movimentacoes = movimentacoes;
    }
}
