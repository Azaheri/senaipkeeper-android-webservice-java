package com.android.info.senai.senaipkeeper.services;

import com.android.info.senai.senaipkeeper.models.ItemPatrimonio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ItemPatrimonioService {
    @GET("rest/itens")
    Call<List<ItemPatrimonio>> listarItens();
}