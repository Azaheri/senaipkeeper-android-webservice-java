package com.android.info.senai.senaipkeeper.models;

import java.io.Serializable;

public class ItemPatrimonio implements Serializable {
    private Long id;

    private Patrimonio patrimonio;

    private Ambiente ambiente;

    private Usuario usuario;

    private String ultimaMovimentacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patrimonio getPatrimonio() {
        return patrimonio;
    }

    public void setPatrimonio(Patrimonio patrimonio) {
        this.patrimonio = patrimonio;
    }

    public Ambiente getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(Ambiente ambiente) {
        this.ambiente = ambiente;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getUltimaMovimentacao() {
        if (ultimaMovimentacao == null || ultimaMovimentacao.isEmpty() || ultimaMovimentacao.equals("") || ultimaMovimentacao.equals("null")) {
            return "Sem movimentações";
        }
        return ultimaMovimentacao;
    }

    public void setUltimaMovimentacao(String ultimaMovimentacao) {
        this.ultimaMovimentacao = ultimaMovimentacao;
    }

    @Override
    public String toString() {
        return "ItemPatrimonio{" +
                "id=" + id +
                ", patrimonio=" + patrimonio +
                ", ambiente=" + ambiente +
                ", usuario=" + usuario +
                ", ultimaMovimentacao=" + ultimaMovimentacao +
                '}';
    }
}
