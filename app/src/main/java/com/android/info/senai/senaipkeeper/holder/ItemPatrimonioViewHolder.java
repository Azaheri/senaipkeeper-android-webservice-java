package com.android.info.senai.senaipkeeper.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.info.senai.senaipkeeper.R;
import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterItem;
import com.android.info.senai.senaipkeeper.models.ItemPatrimonio;

public class ItemPatrimonioViewHolder extends RecyclerView.ViewHolder {


    private final RecyclerAdapterItem adapter;
    private Long id;
    private final TextView idTv;
    private final TextView nome;
    private final TextView data;
    private final TextView usuario;
    private final TextView ambiente;


    public ItemPatrimonioViewHolder(View itemView, RecyclerAdapterItem adapter) {
        super(itemView);
        this.adapter = adapter;
        this.nome = itemView.findViewById(R.id.itemNome);
        this.idTv = itemView.findViewById(R.id.itemId);
        this.data = itemView.findViewById(R.id.itemData);
        this.usuario = itemView.findViewById(R.id.itemUsuario);
        this.ambiente = itemView.findViewById(R.id.ambienteItemPatrimonioId);
    }

    public void preencher(ItemPatrimonio itemPatrimonio) {
        id = itemPatrimonio.getId();
        nome.setText(itemPatrimonio.getPatrimonio().getNome());
        idTv.setText(String.valueOf(itemPatrimonio.getId()));
        data.setText(itemPatrimonio.getUltimaMovimentacao());
        usuario.setText(itemPatrimonio.getUsuario().getNome() + " " + itemPatrimonio.getUsuario().getSobrenome());
        idTv.setText(String.valueOf(id));
        ambiente.setText(itemPatrimonio.getAmbiente().getNome());
    }
}
