package com.android.info.senai.senaipkeeper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.info.senai.senaipkeeper.adapter.RecyclerAdapterItem;
import com.android.info.senai.senaipkeeper.configs.RetrofitConfigItem;
import com.android.info.senai.senaipkeeper.models.ItemPatrimonio;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerLista;
    private RecyclerAdapterItem recyclerAdapter;
    private List<ItemPatrimonio> itens = new ArrayList<>();

    private String tokenUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final SharedPreferences tokenSp = this.getSharedPreferences("token", MODE_PRIVATE);
        tokenUser = tokenSp.getString("tokenUsuario", null);
        Log.e("tokenusuario", tokenUser);

        recyclerLista = findViewById(R.id.rvItem);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerLista.setLayoutManager(linearLayoutManager);
        recyclerAdapter = new RecyclerAdapterItem(getApplicationContext(), itens);
        recyclerLista.setAdapter(recyclerAdapter);

        final Call<List<ItemPatrimonio>> listCall = new RetrofitConfigItem(tokenUser).getItemInterface().listarItens();
        listCall.enqueue(new Callback<List<ItemPatrimonio>>() {
            @Override
            public void onResponse(Call<List<ItemPatrimonio>> call, Response<List<ItemPatrimonio>> response) {
                if (response.isSuccessful()) {
                    itens = response.body();
                    if (itens != null) {
                        List<ItemPatrimonio> aaaaa = response.body();
                        Log.e("sadsadsadsadsadsad", aaaaa.toString());
                        recyclerAdapter.setItemPatrimonios(itens);
                        recyclerAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ItemPatrimonio>> call, Throwable t) {
                t.printStackTrace();
                Log.e("Erro item       ", t.getMessage());
                Toast.makeText(ItemDrawerActivity.this, "Erro, os itens não puderam ser carregados.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_ambiente) {
            Intent intent = new Intent(this, AmbienteDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_movimentacao) {
            Intent intent = new Intent(this, ListaMovimentacaoDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_rMovimentacao) {
            Intent intent = new Intent(this, MovimentacaoDrawerActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_logout) {
            tokenUser = "";
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
