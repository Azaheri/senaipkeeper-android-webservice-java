package com.android.info.senai.senaipkeeper.services;

import com.android.info.senai.senaipkeeper.models.Movimentacao;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MovimentacaoService {
    @GET("rest/itens/{id}/movimentacoes")
    Call<Movimentacao> listarMovimentacoes(@Path("id") Long id);

    @POST("rest/itens/{id}/movimentacoes")
    Call<Movimentacao> realizarMovimentacoes(@Path("id") Long id, @Body Movimentacao ambiente_destino);
}
