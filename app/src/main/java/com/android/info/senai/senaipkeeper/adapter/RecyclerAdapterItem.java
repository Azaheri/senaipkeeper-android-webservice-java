package com.android.info.senai.senaipkeeper.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.info.senai.senaipkeeper.R;
import com.android.info.senai.senaipkeeper.holder.ItemPatrimonioViewHolder;
import com.android.info.senai.senaipkeeper.models.ItemPatrimonio;

import java.util.List;

public class RecyclerAdapterItem extends RecyclerView.Adapter {

    private List<ItemPatrimonio> itens;
    private Context context;

    public RecyclerAdapterItem(Context context, List<ItemPatrimonio> itens) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.lista_de_itens, parent, false);
        ItemPatrimonioViewHolder holder = new ItemPatrimonioViewHolder(view, this);
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemPatrimonioViewHolder holder2 = (ItemPatrimonioViewHolder) holder;
        ItemPatrimonio item = itens.get(position);
        holder2.preencher(item);
    }

    @Override
    public int getItemCount() {
        if (itens == null) {
            return 0;
        }
        return itens.size();
    }

    public void setItemPatrimonios(List<ItemPatrimonio> itens) {
        this.itens = itens;
    }
}
